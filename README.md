# 厚朴的网络日志

你好，欢迎来到我的网络日志，这里记录了我的一些学习、思考和总结，如果不小心帮助了你，我将感到非常荣幸，本站涉及但不限于如下知识点：

* 计算机基础理论知识
  * 操作系统
  * 计算机网络
  * 计算机组成原理
  * 数据结构与算法
* 
* 架构设计
  * 微服务架构
  * 分布式架构
* 
* 数据库设计
  *  Oracle
  *  MySQL
  *  MongoDB
  *  Redis
* 
* 编程语言
  * Java 
  * Python
  * Go 
  * JavaScript
  * PL/SQL
  * Shell Script
  * 正则表达式
* 
* Web开发
  * Http/Https协议
  * TCP/IP协议
  * Spring
  * Spring Boot
  * Spring Cloud
  * MyBatis 
* 
*  中间件
  * Tomcat
  * Nginx 
  * Kafka
* 
* 项目管理
  * PMP
  * 敏捷开发

* 程序员修养
  * Design Patterns
  * Clean Code
  * Code and Talk
